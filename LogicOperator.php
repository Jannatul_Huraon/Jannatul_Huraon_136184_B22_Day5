
<?php
//AND
$x = 150;
$y = 17;

if ($x == 150 and $y == 17) {
    echo "I have a sister" ."<br>";
}


//OR

$x = 34;
$y = 80;

if ($x == 100 or $y == 80) {
    echo "Finish!" ."<br>";
}

//XOR
$x = 10;
$y = 30;

if ($x == 100 xor $y == 80) {
    echo "Bad" ."<br>";
}

//NOT

$x = 35;

if ($x !== 25) {
    echo "No!" . "<br>";
}


//AND
$x = 50;
$y = 15;

if ($x == 50 && $y == 15) {
    echo "Same" . "<br>";
}


//OR
$x = 70;
$y = 20;

if ($x == 70 || $y == 20) {
    echo "Hello";
}
?>
